SAFE JSON ByteStream Parser
===========================

A parser combinator tool to parse JSON byte-streams.

## Code Output:

### dat/error-message-mimic-emacs-hexl-mode.json

```json
[1,2,3,4,5,6,7,8,9,42,"5","\"]
```

```
[T480:~/code/haskell/json]$ cat dat/error-message-mimic-emacs-hexl-mode.json \
> | ./bin/json-bytestream-parser 
# Parser error
* Function:.....: byteP
* Index.........: 0000000000000000
* Unparsed bytes: 30
5B31 2C32 2C33 2C34 2C35 2C36 2C37 2C38  [1,2,3,4,5,6,7,8
2C39 2C34 322C 2235 222C 225C 225D       ,9,42,"5","\"]
…
[T480:~/code/haskell/json]$ 
```

### dat/json.org-example-00.json

```json
{
  "glossary": {
    "title": "example glossary",
    "GlossDiv": {
      "title": "S",
      "GlossList": {
        "GlossEntry": {
          "ID": "SGML",
          "SortAs": "SGML",
          "GlossTerm": "Standard Generalized Markup Language",
          "Acronym": "SGML",
          "Abbrev": "ISO 8879:1986",
          "GlossDef": {
            "para": "A meta-markup language, used to create markup languages such as DocBook.",
            "GlossSeeAlso": [
              "GML",
              "XML"
            ]
          },
          "GlossSee": "markup"
        }
      }
    }
  }
}
```

```
[T480:~/code/haskell/json]$ cat dat/json.org-example-00.json \
> | ./bin/json-bytestream-parser 
Object
  [ ( "glossary"
    , Object
        [ ( "title" , String "example glossary" )
        , ( "GlossDiv"
          , Object
              [ ( "title" , String "S" )
              , ( "GlossList"
                , Object
                    [ ( "GlossEntry"
                      , Object
                          [ ( "ID" , String "SGML" )
                          , ( "SortAs" , String "SGML" )
                          , ( "GlossTerm" , String "Standard Generalized Markup Language" )
                          , ( "Acronym" , String "SGML" )
                          , ( "Abbrev" , String "ISO 8879:1986" )
                          , ( "GlossDef"
                            , Object
                                [ ( "para"
                                  , String
                                      "A meta-markup language, used to create markup languages such as DocBook."
                                  )
                                , ( "GlossSeeAlso" , Array [ String "GML" , String "XML" ] )
                                ]
                            )
                          , ( "GlossSee" , String "markup" )
                          ]
                      )
                    ]
                )
              ]
          )
        ]
    )
  ]
[T480:~/code/haskell/json]$ 
```

### dat/json.org-example-01.json

```json
{
  "menu": {
    "id": "file",
    "value": "File",
    "popup": {
      "menuitem": [
        {
          "value": "New",
          "onclick": "CreateNewDoc()"
        },
        {
          "value": "Open",
          "onclick": "OpenDoc()"
        },
        {
          "value": "Close",
          "onclick": "CloseDoc()"
        }
      ]
    }
  }
}
```

```
[T480:~/code/haskell/json]$ cat dat/json.org-example-01.json \
> | ./bin/json-bytestream-parser 
Object
  [ ( "menu"
    , Object
        [ ( "id" , String "file" )
        , ( "value" , String "File" )
        , ( "popup"
          , Object
              [ ( "menuitem"
                , Array
                    [ Object
                        [ ( "value" , String "New" )
                        , ( "onclick" , String "CreateNewDoc()" )
                        ]
                    , Object
                        [ ( "value" , String "Open" )
                        , ( "onclick" , String "OpenDoc()" )
                        ]
                    , Object
                        [ ( "value" , String "Close" )
                        , ( "onclick" , String "CloseDoc()" )
                        ]
                    ]
                )
              ]
          )
        ]
    )
  ]
[T480:~/code/haskell/json]$ 
```

### dat/json.org-example-02.json

```json
{
  "widget": {
    "debug": "on",
    "window": {
      "title": "Sample Konfabulator Widget",
      "name": "main_window",
      "width": 500,
      "height": 500
    },
    "image": {
      "src": "Images/Sun.png",
      "name": "sun1",
      "hOffset": 250,
      "vOffset": 250,
      "alignment": "center"
    },
    "text": {
      "data": "Click Here",
      "size": 36,
      "style": "bold",
      "name": "text1",
      "hOffset": 250,
      "vOffset": 100,
      "alignment": "center",
      "onMouseUp": "sun1.opacity = (sun1.opacity / 100) * 90;"
    }
  }
}
```

```
[T480:~/code/haskell/json]$ cat dat/json.org-example-02.json \
> | ./bin/json-bytestream-parser 
Object
  [ ( "widget"
    , Object
        [ ( "debug" , String "on" )
        , ( "window"
          , Object
              [ ( "title" , String "Sample Konfabulator Widget" )
              , ( "name" , String "main_window" )
              , ( "width" , Number (500 % 1) )
              , ( "height" , Number (500 % 1) )
              ]
          )
        , ( "image"
          , Object
              [ ( "src" , String "Images/Sun.png" )
              , ( "name" , String "sun1" )
              , ( "hOffset" , Number (250 % 1) )
              , ( "vOffset" , Number (250 % 1) )
              , ( "alignment" , String "center" )
              ]
          )
        , ( "text"
          , Object
              [ ( "data" , String "Click Here" )
              , ( "size" , Number (36 % 1) )
              , ( "style" , String "bold" )
              , ( "name" , String "text1" )
              , ( "hOffset" , Number (250 % 1) )
              , ( "vOffset" , Number (100 % 1) )
              , ( "alignment" , String "center" )
              , ( "onMouseUp"
                , String "sun1.opacity = (sun1.opacity / 100) * 90;"
                )
              ]
          )
        ]
    )
  ]
[T480:~/code/haskell/json]$ 
```

### dat/json.org-example-03.json

```json
{
  "web-app": {
    "servlet": [
      {
        "servlet-name": "cofaxCDS",
        "servlet-class": "org.cofax.cds.CDSServlet",
        "init-param": {
          "configGlossary:installationAt": "Philadelphia, PA",
          "configGlossary:adminEmail": "ksm@pobox.com",
          "configGlossary:poweredBy": "Cofax",
          "configGlossary:poweredByIcon": "/images/cofax.gif",
          "configGlossary:staticPath": "/content/static",
          "templateProcessorClass": "org.cofax.WysiwygTemplate",
          "templateLoaderClass": "org.cofax.FilesTemplateLoader",
          "templatePath": "templates",
          "templateOverridePath": "",
          "defaultListTemplate": "listTemplate.htm",
          "defaultFileTemplate": "articleTemplate.htm",
          "useJSP": false,
          "jspListTemplate": "listTemplate.jsp",
          "jspFileTemplate": "articleTemplate.jsp",
          "cachePackageTagsTrack": 200,
          "cachePackageTagsStore": 200,
          "cachePackageTagsRefresh": 60,
          "cacheTemplatesTrack": 100,
          "cacheTemplatesStore": 50,
          "cacheTemplatesRefresh": 15,
          "cachePagesTrack": 200,
          "cachePagesStore": 100,
          "cachePagesRefresh": 10,
          "cachePagesDirtyRead": 10,
          "searchEngineListTemplate": "forSearchEnginesList.htm",
          "searchEngineFileTemplate": "forSearchEngines.htm",
          "searchEngineRobotsDb": "WEB-INF/robots.db",
          "useDataStore": true,
          "dataStoreClass": "org.cofax.SqlDataStore",
          "redirectionClass": "org.cofax.SqlRedirection",
          "dataStoreName": "cofax",
          "dataStoreDriver": "com.microsoft.jdbc.sqlserver.SQLServerDriver",
          "dataStoreUrl": "jdbc:microsoft:sqlserver://LOCALHOST:1433;DatabaseName=goon",
          "dataStoreUser": "sa",
          "dataStorePassword": "dataStoreTestQuery",
          "dataStoreTestQuery": "SET NOCOUNT ON;select test='test';",
          "dataStoreLogFile": "/usr/local/tomcat/logs/datastore.log",
          "dataStoreInitConns": 10,
          "dataStoreMaxConns": 100,
          "dataStoreConnUsageLimit": 100,
          "dataStoreLogLevel": "debug",
          "maxUrlLength": 500
        }
      },
      {
        "servlet-name": "cofaxEmail",
        "servlet-class": "org.cofax.cds.EmailServlet",
        "init-param": {
          "mailHost": "mail1",
          "mailHostOverride": "mail2"
        }
      },
      {
        "servlet-name": "cofaxAdmin",
        "servlet-class": "org.cofax.cds.AdminServlet"
      },
      {
        "servlet-name": "fileServlet",
        "servlet-class": "org.cofax.cds.FileServlet"
      },
      {
        "servlet-name": "cofaxTools",
        "servlet-class": "org.cofax.cms.CofaxToolsServlet",
        "init-param": {
          "templatePath": "toolstemplates/",
          "log": 1,
          "logLocation": "/usr/local/tomcat/logs/CofaxTools.log",
          "logMaxSize": "",
          "dataLog": 1,
          "dataLogLocation": "/usr/local/tomcat/logs/dataLog.log",
          "dataLogMaxSize": "",
          "removePageCache": "/content/admin/remove?cache=pages&id=",
          "removeTemplateCache": "/content/admin/remove?cache=templates&id=",
          "fileTransferFolder": "/usr/local/tomcat/webapps/content/fileTransferFolder",
          "lookInContext": 1,
          "adminGroupID": 4,
          "betaServer": true
        }
      }
    ],
    "servlet-mapping": {
      "cofaxCDS": "/",
      "cofaxEmail": "/cofaxutil/aemail/*",
      "cofaxAdmin": "/admin/*",
      "fileServlet": "/static/*",
      "cofaxTools": "/tools/*"
    },
    "taglib": {
      "taglib-uri": "cofax.tld",
      "taglib-location": "/WEB-INF/tlds/cofax.tld"
    }
  }
}
```

```
[T480:~/code/haskell/json]$ cat dat/json.org-example-03.json \
> | ./bin/json-bytestream-parser 
Object
  [ ( "web-app"
    , Object
        [ ( "servlet"
          , Array
              [ Object
                  [ ( "servlet-name" , String "cofaxCDS" )
                  , ( "servlet-class" , String "org.cofax.cds.CDSServlet" )
                  , ( "init-param"
                    , Object
                        [ ( "configGlossary:installationAt" , String "Philadelphia, PA" )
                        , ( "configGlossary:adminEmail" , String "ksm@pobox.com" )
                        , ( "configGlossary:poweredBy" , String "Cofax" )
                        , ( "configGlossary:poweredByIcon" , String "/images/cofax.gif" )
                        , ( "configGlossary:staticPath" , String "/content/static" )
                        , ( "templateProcessorClass" , String "org.cofax.WysiwygTemplate" )
                        , ( "templateLoaderClass"
                          , String "org.cofax.FilesTemplateLoader"
                          )
                        , ( "templatePath" , String "templates" )
                        , ( "templateOverridePath" , String "" )
                        , ( "defaultListTemplate" , String "listTemplate.htm" )
                        , ( "defaultFileTemplate" , String "articleTemplate.htm" )
                        , ( "useJSP" , Boolean False )
                        , ( "jspListTemplate" , String "listTemplate.jsp" )
                        , ( "jspFileTemplate" , String "articleTemplate.jsp" )
                        , ( "cachePackageTagsTrack" , Number (200 % 1) )
                        , ( "cachePackageTagsStore" , Number (200 % 1) )
                        , ( "cachePackageTagsRefresh" , Number (60 % 1) )
                        , ( "cacheTemplatesTrack" , Number (100 % 1) )
                        , ( "cacheTemplatesStore" , Number (50 % 1) )
                        , ( "cacheTemplatesRefresh" , Number (15 % 1) )
                        , ( "cachePagesTrack" , Number (200 % 1) )
                        , ( "cachePagesStore" , Number (100 % 1) )
                        , ( "cachePagesRefresh" , Number (10 % 1) )
                        , ( "cachePagesDirtyRead" , Number (10 % 1) )
                        , ( "searchEngineListTemplate"
                          , String "forSearchEnginesList.htm"
                          )
                        , ( "searchEngineFileTemplate" , String "forSearchEngines.htm" )
                        , ( "searchEngineRobotsDb" , String "WEB-INF/robots.db" )
                        , ( "useDataStore" , Boolean True )
                        , ( "dataStoreClass" , String "org.cofax.SqlDataStore" )
                        , ( "redirectionClass" , String "org.cofax.SqlRedirection" )
                        , ( "dataStoreName" , String "cofax" )
                        , ( "dataStoreDriver"
                          , String "com.microsoft.jdbc.sqlserver.SQLServerDriver"
                          )
                        , ( "dataStoreUrl"
                          , String
                              "jdbc:microsoft:sqlserver://LOCALHOST:1433;DatabaseName=goon"
                          )
                        , ( "dataStoreUser" , String "sa" )
                        , ( "dataStorePassword" , String "dataStoreTestQuery" )
                        , ( "dataStoreTestQuery"
                          , String "SET NOCOUNT ON;select test='test';"
                          )
                        , ( "dataStoreLogFile"
                          , String "/usr/local/tomcat/logs/datastore.log"
                          )
                        , ( "dataStoreInitConns" , Number (10 % 1) )
                        , ( "dataStoreMaxConns" , Number (100 % 1) )
                        , ( "dataStoreConnUsageLimit" , Number (100 % 1) )
                        , ( "dataStoreLogLevel" , String "debug" )
                        , ( "maxUrlLength" , Number (500 % 1) )
                        ]
                    )
                  ]
              , Object
                  [ ( "servlet-name" , String "cofaxEmail" )
                  , ( "servlet-class" , String "org.cofax.cds.EmailServlet" )
                  , ( "init-param"
                    , Object
                        [ ( "mailHost" , String "mail1" )
                        , ( "mailHostOverride" , String "mail2" )
                        ]
                    )
                  ]
              , Object
                  [ ( "servlet-name" , String "cofaxAdmin" )
                  , ( "servlet-class" , String "org.cofax.cds.AdminServlet" )
                  ]
              , Object
                  [ ( "servlet-name" , String "fileServlet" )
                  , ( "servlet-class" , String "org.cofax.cds.FileServlet" )
                  ]
              , Object
                  [ ( "servlet-name" , String "cofaxTools" )
                  , ( "servlet-class" , String "org.cofax.cms.CofaxToolsServlet" )
                  , ( "init-param"
                    , Object
                        [ ( "templatePath" , String "toolstemplates/" )
                        , ( "log" , Number (1 % 1) )
                        , ( "logLocation"
                          , String "/usr/local/tomcat/logs/CofaxTools.log"
                          )
                        , ( "logMaxSize" , String "" )
                        , ( "dataLog" , Number (1 % 1) )
                        , ( "dataLogLocation"
                          , String "/usr/local/tomcat/logs/dataLog.log"
                          )
                        , ( "dataLogMaxSize" , String "" )
                        , ( "removePageCache"
                          , String "/content/admin/remove?cache=pages&id="
                          )
                        , ( "removeTemplateCache"
                          , String "/content/admin/remove?cache=templates&id="
                          )
                        , ( "fileTransferFolder"
                          , String "/usr/local/tomcat/webapps/content/fileTransferFolder"
                          )
                        , ( "lookInContext" , Number (1 % 1) )
                        , ( "adminGroupID" , Number (4 % 1) )
                        , ( "betaServer" , Boolean True )
                        ]
                    )
                  ]
              ]
          )
        , ( "servlet-mapping"
          , Object
              [ ( "cofaxCDS" , String "/" )
              , ( "cofaxEmail" , String "/cofaxutil/aemail/*" )
              , ( "cofaxAdmin" , String "/admin/*" )
              , ( "fileServlet" , String "/static/*" )
              , ( "cofaxTools" , String "/tools/*" )
              ]
          )
        , ( "taglib"
          , Object
              [ ( "taglib-uri" , String "cofax.tld" )
              , ( "taglib-location" , String "/WEB-INF/tlds/cofax.tld" )
              ]
          )
        ]
    )
  ]
[T480:~/code/haskell/json]$ 
```

### dat/json.org-example-04.json

```json
{
  "menu": {
    "header": "SVG Viewer",
    "items": [
      {
        "id": "Open"
      },
      {
        "id": "OpenNew",
        "label": "Open New"
      },
      null,
      {
        "id": "ZoomIn",
        "label": "Zoom In"
      },
      {
        "id": "ZoomOut",
        "label": "Zoom Out"
      },
      {
        "id": "OriginalView",
        "label": "Original View"
      },
      null,
      {
        "id": "Quality"
      },
      {
        "id": "Pause"
      },
      {
        "id": "Mute"
      },
      null,
      {
        "id": "Find",
        "label": "Find..."
      },
      {
        "id": "FindAgain",
        "label": "Find Again"
      },
      {
        "id": "Copy"
      },
      {
        "id": "CopyAgain",
        "label": "Copy Again"
      },
      {
        "id": "CopySVG",
        "label": "Copy SVG"
      },
      {
        "id": "ViewSVG",
        "label": "View SVG"
      },
      {
        "id": "ViewSource",
        "label": "View Source"
      },
      {
        "id": "SaveAs",
        "label": "Save As"
      },
      null,
      {
        "id": "Help"
      },
      {
        "id": "About",
        "label": "About Adobe CVG Viewer..."
      }
    ]
  }
}
```

```
[T480:~/code/haskell/json]$ cat dat/json.org-example-04.json \
> | ./bin/json-bytestream-parser 
Object
  [ ( "menu"
    , Object
        [ ( "header" , String "SVG Viewer" )
        , ( "items"
          , Array
              [ Object [ ( "id" , String "Open" ) ]
              , Object
                  [ ( "id" , String "OpenNew" ) , ( "label" , String "Open New" ) ]
              , Null
              , Object
                  [ ( "id" , String "ZoomIn" ) , ( "label" , String "Zoom In" ) ]
              , Object
                  [ ( "id" , String "ZoomOut" ) , ( "label" , String "Zoom Out" ) ]
              , Object
                  [ ( "id" , String "OriginalView" )
                  , ( "label" , String "Original View" )
                  ]
              , Null
              , Object [ ( "id" , String "Quality" ) ]
              , Object [ ( "id" , String "Pause" ) ]
              , Object [ ( "id" , String "Mute" ) ]
              , Null
              , Object
                  [ ( "id" , String "Find" ) , ( "label" , String "Find..." ) ]
              , Object
                  [ ( "id" , String "FindAgain" )
                  , ( "label" , String "Find Again" )
                  ]
              , Object [ ( "id" , String "Copy" ) ]
              , Object
                  [ ( "id" , String "CopyAgain" )
                  , ( "label" , String "Copy Again" )
                  ]
              , Object
                  [ ( "id" , String "CopySVG" ) , ( "label" , String "Copy SVG" ) ]
              , Object
                  [ ( "id" , String "ViewSVG" ) , ( "label" , String "View SVG" ) ]
              , Object
                  [ ( "id" , String "ViewSource" )
                  , ( "label" , String "View Source" )
                  ]
              , Object
                  [ ( "id" , String "SaveAs" ) , ( "label" , String "Save As" ) ]
              , Null
              , Object [ ( "id" , String "Help" ) ]
              , Object
                  [ ( "id" , String "About" )
                  , ( "label" , String "About Adobe CVG Viewer..." )
                  ]
              ]
          )
        ]
    )
  ]
[T480:~/code/haskell/json]$ 
```


## References:

* JSON.org:
  - [Introducing][introducing]
  - [Examples][examples]
* Wikipedia (ASCII):
  - [Control characters][controlchars]
* Haskell Hackage:
  - [Data.ByteString.Lazy][bytestring]
  - [Text.Show.Pretty][prettyshow]
* YouTube - Tsoding channel:
  - [JSON Parser 100% From Scratch in Haskell (only 111 lines)][tsoding]
* JSONformatter.org:
  - [Prettify][prettify]
* GitLab - SPISE MISU ApS:
  - [spisemisu/json-bytestream-parser][spisemisu]

[introducing]:  https://www.json.org/json-en.html
[examples]:     https://json.org/example.html
[controlchars]: https://en.wikipedia.org/wiki/ASCII#Control_characters
[bytestring]:   https://hackage.haskell.org/package/bytestring/docs/Data-ByteString-Lazy.html
[prettyshow]:   https://hackage.haskell.org/package/pretty-show/docs/Text-Show-Pretty.html#v:ppShow
[tsoding]:      https://www.youtube.com/watch?v=N9RUqGYuGfw
[prettify]:     https://jsonformatter.org/
[spisemisu]:    https://gitlab.com/spisemisu/json-bytestream-parser
