--------------------------------------------------------------------------------
--
-- Byte.Parser.JSON, (c) 2020 SPISE MISU ApS
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Byte.Parser.JSON
  ( jsonP
  , stringP, numberP, objectP, arrayP, boolP, nullP
  )
where

--------------------------------------------------------------------------------

import           Data.Char
  ( chr
  )

import           Byte.Parser
  ( Byte
  , Parser
  , byteP
  , bytesP
  , chunkP
  , failP
  , getP
  , many
  , peekP
  , sepBy
  , spanP
  , (<|>)
  )
import qualified Data.JSON   as JSON

--------------------------------------------------------------------------------

jsonP
  :: Parser JSON.Value
jsonP =
  stringP <|> numberP <|> objectP <|> arrayP <|> boolP <|> nullP

stringP
  :: Parser JSON.Value
stringP =
  JSON.String <$> charsP

numberP
  :: Parser JSON.Value
numberP =
  JSON.Number . toRational . s2d . map (chr . fromIntegral) <$> aux
  where
    aux = sci <|> dec <|> int
    sci =
      d <|> i
      where
        d = cmb <$> dec <*> (byteP cce <|> byteP lce) <*> num
        i = cmb <$> int <*> (byteP cce <|> byteP lce) <*> num
    dec =
      n <|> d
      where
        n = (:) <$>         byteP neg <*> d
        d = cmb <$> num <*> byteP dot <*> num
    int =
      n <|> num
      where
        n = (:) <$> byteP neg <*> num
    neg = 045
    dot = 046
    cce = 069
    lce = 101
    num = spanP (\b -> 47 < b && b < 58)
    s2d = read :: String -> Double
    cmb = \xs y zs -> xs ++ [y] ++ zs

objectP
  :: Parser JSON.Value
objectP =
  JSON.Object <$> aux
  where
    aux =
      oc *> ws *> ps `sepBy` (ws *> cs <* ws) <* ws <* cc
      where
        ps =
          (\k _ v -> (k,v))
          <$> charsP
          <*> (ws *> kv <* ws)
          <*> jsonP
        oc = byteP 123
        cc = byteP 125
        cs = byteP 044
        kv = byteP 058
        ws = many  whitespaceP

arrayP
  :: Parser JSON.Value
arrayP =
  JSON.Array <$> aux
  where
    aux =
      ob *> ws *> jsonP `sepBy` (ws *> cs <* ws) <* ws <* cb
      where
        ob = byteP 091
        cb = byteP 093
        cs = byteP 044
        ws = many  whitespaceP

boolP
  :: Parser JSON.Value
boolP =
  trueP <|> falseP

nullP
  :: Parser JSON.Value
nullP =
  aux <$> bytesP [110,117,108,108]
  where
    aux _ = JSON.Null

--------------------------------------------------------------------------------

-- HELPERS

trueP
  :: Parser JSON.Value
trueP =
  aux <$> bytesP [116,114,117,101]
  where
    aux _ = JSON.Boolean True

falseP
  :: Parser JSON.Value
falseP =
  aux <$> bytesP [102,97,108,115,101]
  where
    aux _ = JSON.Boolean False

whitespaceP
  :: Parser Byte
whitespaceP =
 ht <|> lf <|> cr <|> sb
 where
   ht = byteP 009
   lf = byteP 010
   cr = byteP 013
   sb = byteP 032

charsP
  :: Parser [Char]
charsP =
  map (chr . fromIntegral)
  <$> (byteP 034 *> aux <* byteP 034)
  where
    aux =
      peekP n >>= \ bs ->
      case bs of
        [   ] -> pure []
        -- Reverse slash: '\\'
        [092] -> esc
        (b:_) ->
          if   g b
          then pure []
          else f
        where
          n = 1
          f = (:) <$> getP <*> aux
          g =
            \ b ->
              -- All except
              b <  032 || -- Control Codes
              b == 127 || -- Delete: '\DEL'
              b == 034    -- Quotation Mark
    esc =
      peekP n >>= \ bs ->
      case bs of
        [092     ] -> failP "charsP > invalid single reverse slash"
        -- Escaped backspace: '\b'
        [092, 098] -> f
        -- Escaped horizontal tab: '\t'
        [092, 116] -> f
        -- Escaped line feed: '\n
        [092, 110] -> f
        -- Escaped form feed: '\f'
        [092, 102] -> f
        -- Escaped carriage return: '\r
        [092, 114] -> f
        -- Escaped double quotes: '\\':'"'
        [092, 034] -> f
        -- Escaped forward slash: '\\':'/'
        [092, 047] -> f
        -- Escaped reverse slash: '\\':'\\'
        [092, 092] -> f
        [092, 117] -> uni
        __________ -> failP "charsP > invalid reverse slash sequence"
      where
        n = 2
        f = (++) <$> chunkP n <*> aux
    uni =
      peekP n >>= \ bs ->
      case bs of
        -- Unicode: '\\':'u':'0':'0':'0':'0' - '\\':'u':'F':'F':'F':'F'
        092:117:xs | length xs == 4 && all f xs -> g
        _______________________________________ -> failP e
      where
        n = 6
        f =
          \ b ->
            (047 < b && b < 058) || -- [ 0 .. 9 ]
            (064 < b && b < 071) || -- ['A'..'F']
            (092 < b && b < 103)    -- ['a'..'f']
        g = (++) <$> chunkP n <*> aux
        e = "charsP > invalid hex unicode"
