--------------------------------------------------------------------------------
--
-- Byte.Parser, (c) 2020 SPISE MISU ApS
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Byte.Parser
  ( Byte, Bytes
  , Output
    ( output
    )
  , Parser
    ( parse
    )
  , empty, many, some, (<|>)
  , sepBy
  , peekP
  , failP, spanP
  , getP
  , byteP, bytesP, chunkP
  , run
  )
where

--------------------------------------------------------------------------------

import           Control.Applicative
  ( Alternative
  , empty
  , many
  , some
  , (<|>)
  )
import           Data.Bits
  ( Bits
  , shiftL
  , shiftR
  )
import           Data.Char
  ( chr
  )
import           Data.Word
  ( Word8
  )

--------------------------------------------------------------------------------

type Byte  = Word8
type Bytes = [Byte]

type Index = Word
type Error = String

--------------------------------------------------------------------------------

newtype Output a = O
  { output :: (Either (Index, Error) (Index, a, Bytes))
  }

newtype Parser a = P
  { parse :: Index -> Bytes -> Output a
  }

--------------------------------------------------------------------------------

instance Show a => Show (Output a) where
  show o =
    case ppp "show" o of
      Left  e -> e
      Right a -> show a

--------------------------------------------------------------------------------

instance Functor Output where
  fmap _ (O (Left        e))  = O $ Left          e
  fmap f (O (Right (i,a,bs))) = O $ Right $ (i, f a, bs)

instance Applicative Output where
  pure          a           = O $ Right (0,   a, [])
  O (Left       e)  <*>   _ = O $ Left        e
  O (Right (_,f,_)) <*> O r =
    case r of
      Left      e      -> O $ Left        e
      Right (i, a, bs) -> O $ Right (i, f a, bs)

instance Alternative Output where
  empty             = O $ Left (0, [])
  O (Left  _) <|> o = o
  o           <|> _ = o

{-

instance Semigroup (Output a) where
  O (Left _) <> r = r
  l          <> _ = l

instance Foldable Output where
  foldMap _ (O (Left       _))  = mempty
  foldMap f (O (Right (_,a,_))) = f a

instance Monad Output where
  O (Left       e)  >>= _ = O (Left e)
  O (Right (_,a,_)) >>= f = f a
-}

--------------------------------------------------------------------------------

instance Functor Parser where
  fmap f (P p) = P $ \ n bs ->
    case p n bs of
      O (Left  (i, e))     -> O $ Left  (i,   e)
      O (Right (i, a, xs)) -> O $ Right (i, f a, xs)

instance Applicative Parser where
  pure a              = P $ \ n bs -> O $ Right (n, a, bs)
  (<*>) (P p1) (P p2) = P $ \ n bs ->
    case p1 n bs of
      O (Left  (i, e))         -> O $ Left (i,    e)
      O (Right (i, f, xs))     ->
        case p2 i xs of
          O (Left  (j, e))     -> O $ Left  (j,   e)
          O (Right (j, a, ys)) -> O $ Right (j, f a, ys)

instance Monad Parser where
  (>>=) (P p) f = P $ \ n bs ->
    case p n bs of
      O (Left  (i, e))     -> O $ Left (i, e)
      O (Right (i, a, xs)) -> parse (f a) i xs

instance Alternative Parser where
  empty               = P $ \ i __ -> O $ Left (i, [])
  (<|>) (P p1) (P p2) = P $ \ n bs -> p1 n bs <|> p2 n bs

--------------------------------------------------------------------------------

sepBy
  :: (Alternative f)
  => f a
  -> f b
  -> f [a]
sepBy p sep =
  aux <|> pure []
  where
    aux = (:) <$> p <*> (many $ sep *> p)

--------------------------------------------------------------------------------

peekP
  :: Int
  -> Parser Bytes
peekP n =
  P $ \ i bs -> O $ Right (i, take n bs, bs)

failP
  :: Error
  -> Parser a
failP msg =
  P $ \ i bs -> O $ Left (i, err i msg bs)

spanP
  :: (Byte -> Bool)
  -> Parser Bytes
spanP f =
  P $ \ i bs -> aux i bs
  where
    aux i bs =
      case span f bs of
        ([],__) -> O $ Left  (i, err i "spanP" bs)
        (as,rs) -> O $ Right (i, as, rs)

getP
  :: Parser Byte
getP =
  P $ \ i bs -> aux i bs
  where
    aux i [    ] = O $ Left  (i, err i "getP" [])
    aux i (x:rs) = O $ Right (i+1, x, rs)

byteP
  :: Byte
  -> Parser Byte
byteP b =
  P $ \ i bs -> aux i bs
  where
    aux i    [    ] = O $ Left (i, err i "getP" [])
    aux i bs@(x:rs) =
      if b == x
      then            O $ Right (i+1, x, rs)
      else            O $ Left  (i, err i "byteP" bs)

bytesP
  :: Bytes
  -> Parser Bytes
bytesP =
  sequenceA . map byteP

chunkP
  :: Integral a
  => a
  -> Parser Bytes
chunkP n =
  P $ \ i bs ->
    case take m bs of
      xs | length xs == m -> O $ Right (i+j, take m xs, drop m bs)
      ___________________ -> O $ Left  (i, err i "chunkP" bs)
  where
    j = fromIntegral n
    m = fromIntegral n

--------------------------------------------------------------------------------

run
  :: Parser a
  -> Bytes
  -> Either Error a
run p =
  ppp "run" . parse p 0

--------------------------------------------------------------------------------

-- HELPERS

ppp
  :: String
  -> Output a
  -> Either Error a
ppp _ (O (Left  (_,e)))    = Left  $ e
ppp _ (O (Right (_,a,[]))) = Right a
ppp m (O (Right (i,_,bs))) = Left  $ err i ("ppp > " ++ m) bs

(.<.)
  :: Bits a
  => a
  -> Int
  -> a
(.<.) x y = x `shiftL` y

(.>.)
  :: Bits a
  => a
  -> Int
  -> a
(.>.) x y = x `shiftR` y

n2b
  :: (Bits a, Integral a)
  => Int
  -> (a -> a)
  -> a
  -> [Byte]
n2b b f =
  aux []
  where
    aux [ ] 0 = [ fromIntegral $! f 0 ]
    aux acc 0 = acc
    aux acc n =
      aux (r : acc) c
      where
        c =                   n         .>. b
        r = fromIntegral $ f (n - c * 1 .<. b)

b2h
  :: Word8
  -> [Char]
b2h =
  pad True 2 '0' . map (chr . fromIntegral) .  n2b 4 {- 2^4 = 016 -} aux
  where
    aux n
      | n <= 0x09 = 48 + n
      | otherwise = 55 + n

pad :: Bool -> Int -> Char -> String -> String
pad d n c x =
  if l > n
  then x
  else
    if d
    then replicate (n-l) c ++ x
    else x ++ replicate (n-l) c
  where
    l = length x

cof
  :: Int
  ->  [a]
  -> [[a]]
cof _ [] = [            ]
cof n xs = y : (cof n ys)
  where
    (y,ys) = splitAt n xs

err
  :: Index
  -> String
  -> [Word8]
  -> String
err i f bs =
  "# Parser error"                                 ++ "\n" ++
  "* Function:.....: " ++ f                        ++ "\n" ++
  "* Index.........: " ++ pad True 16 '0' (show i) ++ "\n" ++
  "* Unparsed bytes: " ++ show m                   ++ "\n" ++ out bs
  where
    m = length bs
    out xs = -- mimic `hexl-mode` in `emacs`
      (++ "…") $
      foldl (\a (x,y) -> a ++ (rp y) ++ " " ++ x ++ "\n") [] . zip zs . cof 40 $
      foldl (\a  x    -> a ++     x  ++ " ")              []          . cof 04 $
      concat   $
      map  b2h $
      ys
      where
        rp = pad False 40 ' '
        ys = take 640          xs
        zs = cof  016 $ map ec ys
        ec =
          \ b ->
            if b < 032 || b == 127
            then '.'
            else chr $ fromIntegral b
