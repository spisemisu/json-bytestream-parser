--------------------------------------------------------------------------------
--
-- Data.JSON, (c) 2020 SPISE MISU ApS
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.JSON
  ( Value
    ( String
    , Number
    , Object
    , Array
    , Boolean
    , Null
    )
  , DTO
    ( encode
    , decode
    )
  )
where

--------------------------------------------------------------------------------

type Error = String

--------------------------------------------------------------------------------

data Value
  = String  String
  | Number  Rational
  | Object  [(String, Value)]
  | Array   [Value]
  | Boolean Bool
  | Null
  deriving
    ( Read
    , Show
    )

--------------------------------------------------------------------------------

class DTO a where
  encode :: a     -> Value
  decode :: Value -> Either Error a
