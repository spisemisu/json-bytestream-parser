--------------------------------------------------------------------------------
--
-- JsonByteStream, (c) 2020 SPISE MISU ApS
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy as LBS
import           Text.Show.Pretty
  ( ppShow
  )

import qualified Byte.Parser          as Parser
import qualified Byte.Parser.JSON     as JSON

--------------------------------------------------------------------------------

main :: IO ()
main =
  LBS.getContents >>= putStrLn . aux . eof . LBS.unpack
  where
    eof [   ]  = [      ]
    eof [010]  = [      ]
    eof (b:bs) = b:eof bs
    aux bs =
      case Parser.run JSON.jsonP bs of
        Right ast -> ppShow ast
        Left  err -> err
